﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Flagwind.Storages.Launcher")]
[assembly: AssemblyDescription("Flagwind Storages Server")]
[assembly: AssemblyCompany("Flagwind Corporation")]
[assembly: AssemblyProduct("Flagwind Storages Server")]
[assembly: AssemblyCopyright("Copyright(C) Flagwind Corporation 2017. All rights reserved.")]
[assembly: ComVisible(false)]

[assembly: AssemblyVersion("1.1.0.0505")]
[assembly: AssemblyFileVersion("1.1.0.0505")]
