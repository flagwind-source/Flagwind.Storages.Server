﻿using System;
using System.Web.Mvc;

namespace Flagwind.Storages.Launcher
{
	[HandleError]
	public class DefaultController : Controller
	{
		public ActionResult Index()
		{
			this.ViewData["Now"] = DateTime.Now;
			this.ViewData["Random"] = Flagwind.Common.RandomGenerator.GenerateInt32();
			this.ViewData["Message"] = "Welcome to ASP.NET MVC on Flagwind.Plugins™";
			this.ViewData["PluginTree"] = Flagwind.Plugins.Application.Context.PluginContext.PluginTree;

			return View();
		}
	}
}
