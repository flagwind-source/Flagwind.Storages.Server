﻿using System;
using System.Web.Mvc;

namespace Flagwind.Storages.Launcher
{
	public class MvcApplication : System.Web.HttpApplication
	{
		protected void Application_Start()
		{
			Flagwind.Plugins.Application.Started += Application_Started;
			Flagwind.Plugins.Application.Start(Flagwind.Web.Plugins.ApplicationContext.Current, null);
		}

		protected void Application_End(object sender, EventArgs e)
		{
			Flagwind.Plugins.Application.Exit();
		}

		private void Application_Started(object sender, Flagwind.Plugins.ApplicationEventArgs e)
		{
			var context = Flagwind.Plugins.Application.Context;

			//将应用上下文对象保存到ASP.NET的全局应用缓存容器中
			Application["ApplicationContext"] = context;

			//注册主页的控制器
			context.PluginContext.PluginTree.Mount("/Workspace/Controllers/Home", new Func<IController>(() => new DefaultController()));

			//卸载主题表单构件
//			context.PluginContext.PluginTree.Unmount(Flagwind.Plugins.PluginPath.Combine(context.PluginContext.Settings.WorkbenchPath, "__ThemeForm__"));

			//注销插件应用的启动完成事件的通知
			Flagwind.Plugins.Application.Started -= Application_Started;
		}
	}
}