﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Flagwind.Storages.Web")]
[assembly: AssemblyDescription("Flagwind Storages Web")]
[assembly: AssemblyCompany("Flagwind Corporation")]
[assembly: AssemblyProduct("Flagwind Storages Server")]
[assembly: AssemblyCopyright("Copyright(C) Flagwind Corporation 2017. All rights reserved.")]
[assembly: ComVisible(false)]

[assembly: Guid("d24f5bd6-c51d-4ad2-8511-12b4e81164e5")]
[assembly: AssemblyVersion("1.0.0.303")]
[assembly: AssemblyFileVersion("1.0.0.303")]
