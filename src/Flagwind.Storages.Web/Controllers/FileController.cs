﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Flagwind.Storages.Web.Controllers
{
	public class FileController : ApiController
	{
		#region 成员字段

		private RemoteFileAccessor _accessor;

		#endregion

		#region 构造方法

		public FileController()
		{
			_accessor = new RemoteFileAccessor();
		}

		#endregion

		#region 公共属性

		public string BasePath
		{
			get
			{
				return _accessor.BasePath;
			}
			set
			{
				_accessor.BasePath = value;
			}
		}

		#endregion

		#region 公共方法

		/// <summary>
		/// 下载指定路径的文件。
		/// </summary>
		/// <param name="path">指定要下载的文件的相对路径或绝对路径（绝对路径以/斜杠打头）。</param>
		/// <param name="w">指定要下载的图片的宽度。</param>
		/// <param name="h">指定要下载的图片的高度。</param>
		/// <param name="thumb">指定要下载的图片是缩放或者裁剪。 0 缩放；1 裁剪(能等比缩放就缩放)；2 强制裁剪 </param>
		/// <param name="site">指定要裁剪的图片从哪里开始裁剪 0中间；1左边；2右边</param> 
		public HttpResponseMessage Get(string path, int? w = null, int? h = null, int? thumb = null, int? site = null)
		{
			if(string.IsNullOrWhiteSpace(path))
				throw new HttpResponseException(HttpStatusCode.BadRequest);

            // 读取图片路径
		    if(Utility.IsImagePath(path))
		        return _accessor.ReadImage(path, w, h, thumb, site);

		    return _accessor.Read(path);
		}

		/// <summary>
		/// 获取指定文件的外部访问路径。
		/// </summary>
		/// <param name="path">指定的文件相对路径或绝对路径（绝对路径以/斜杠打头）。</param>
		/// <returns>返回指定文件的外部访问路径。</returns>
		[HttpGet]
		public string Path(string path)
		{
			if(string.IsNullOrWhiteSpace(path))
				throw new HttpResponseException(HttpStatusCode.BadRequest);

			return _accessor.GetUrl(path);
		}

		/// <summary>
		/// 获取指定路径的文件描述信息。
		/// </summary>
		/// <param name="path">指定要获取的文件的相对路径或绝对路径（绝对路径以/斜杠打头）。</param>
		/// <returns>返回的指定的文件详细信息。</returns>
		[HttpGet]
		public Task<Flagwind.IO.FileInfo> Info(string path)
		{
			if(string.IsNullOrWhiteSpace(path))
				throw new HttpResponseException(HttpStatusCode.BadRequest);

			return _accessor.GetInfo(path);
		}

		/// <summary>
		/// 删除指定相对路径的文件。
		/// </summary>
		/// <param name="path">指定要删除的文件的相对路径或绝对路径（绝对路径以/斜杠打头）。</param>
		public Task<bool> Delete(string path)
		{
			if(string.IsNullOrWhiteSpace(path))
				throw new HttpResponseException(HttpStatusCode.BadRequest);

			return _accessor.Delete(path);
		}

		/// <summary>
		/// 修改指定路径的文件描述信息。
		/// </summary>
		/// <param name="path">指定要修改的文件相对路径或绝对路径（绝对路径以/斜杠打头）。</param>
		public Task<bool> Put(string path)
		{
			if(string.IsNullOrWhiteSpace(path))
				throw new HttpResponseException(HttpStatusCode.BadRequest);

			return _accessor.SetInfo(this.Request, path);
		}

		/// <summary>
		/// 上传一个文件或多个文件。
		/// </summary>
		/// <param name="directory">指定上传文件的目录路径（绝对路径以/斜杠打头）。</param>
		/// <returns>返回上传成功的<see cref="Flagwind.IO.FileInfo"/>文件描述信息实体对象集。</returns>
		public Task<IEnumerable<Flagwind.IO.FileInfo>> Post(string directory = null)
		{
			return _accessor.Write(this.Request, directory);
		}

		#endregion
	}
}
